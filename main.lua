require 'torch'
require 'nn'
require 'image'
require 'optim'
require 'os'

require 'loadcaffe'


local cmd = torch.CmdLine()

-- Basic options
cmd:option('-style_image', 'examples/inputs/seated-nude.jpg',
'Style target image')
cmd:option('-style_blend_weights', 'nil')
cmd:option('-content_image', 'examples/inputs/tubingen.jpg',
'Content target image')
cmd:option('-image_size', 512, 'Maximum height / width of generated image')
cmd:option('-gpu', 0, 'Zero-indexed ID of the GPU to use; for CPU mode set -gpu = -1')

-- Optimization options
cmd:option('-content_weight', 5e0)
cmd:option('-style_weight', 1e2)
cmd:option('-tv_weight', 1e-3)
cmd:option('-num_iterations', 1000)
cmd:option('-normalize_gradients', false)
cmd:option('-init', 'random', 'random|image')
cmd:option('-optimizer', 'lbfgs', 'lbfgs|adam')
cmd:option('-learning_rate', 1e1)

-- Output options
cmd:option('-print_iter', 50)
cmd:option('-save_iter', 100)
cmd:option('-output_image', 'out.png')

-- Other options
cmd:option('-style_scale', 1.0)
cmd:option('-original_colors', 0)
cmd:option('-pooling', 'max', 'max|avg')
cmd:option('-proto_file', 'models/VGG_ILSVRC_19_layers_deploy.prototxt')
cmd:option('-model_file', 'models/VGG_ILSVRC_19_layers.caffemodel')
cmd:option('-backend', 'nn', 'nn|cudnn|clnn')
cmd:option('-cudnn_autotune', false)
cmd:option('-seed', -1)

cmd:option('-content_layers', 'relu4_2', 'layers for content')
cmd:option('-style_layers', 'relu1_1,relu2_1,relu3_1,relu4_1,relu5_1', 'layers for style')

cmd:option('-expand_size', 0.0)

function to_command(params)
	local str = cmd:string("", params, {})
	local new_params = str:split(',')
	str = "th neural_style.lua"
	for _, s in ipairs(new_params) do
		value = s:split('=')
		str = str .. " -" .. value[1] .. " " .. value[2]
	end
	return str
end

function split(style_image_file)
	local style_image = image.load(style_image_file, 3)
	local style_image_list = {}
	n = style_image:size(2)
	m = style_image:size(3)
	min_std_error = 1e60
	for i = 1, n / 2 do
		print(i)
		for j = 1, m / 2 do
			mean = style_image[{{}, {i, i + n / 2}, {j, j + m / 2}}]:mean(2):mean(3)
			std_error = style_image[{{}, {i, i + n / 2}, {j, j + m / 2}}]:std(2):std(3)
			if std_error:sum() < min_std_error then
				min_std_error = std_error:sum()
			end
			if std_error:sum() <= 0.1 then
				flag = false
				for col, data in pairs(style_image_list) do
					temp = 0
					x = mean - col
					x:apply(function(x) temp = temp + x * x return x end)
					if temp < 0.03 then
						flag = true
						prev_error = data[3]
						if std_error:sum() < prev_error:sum() then
							style_image_list[col] = nil
							style_image_list[mean] = {i, j, std_error}
						end
						break
					end
				end
				if flag == false then
					style_image_list[mean] = {i, j, std_error}
				end
			end
		end
	end
	local return_list = {}
	local i = 0
	for col, data in pairs(style_image_list) do
		x = data[1]
		y = data[2]
		i = i + 1
		local ext = paths.extname(style_image_file)
		local basename = paths.basename(style_image_file, ext)
		local directory = paths.dirname(style_image_file)
		local new_image_filename = string.format("%s/%s_%d.%s", directory, basename, i, ext)
		image.save(new_image_filename, style_image[{{}, {x, x + n / 2}, {y, y + m / 2}}])
		new_image_filename = string.format("%s_%d.%s", basename, i, ext)
		table.insert(return_list, new_image_filename)
	end
	print('min', min_std_error)
	print('size', #return_list, #style_image_list)
	return return_list
end

local params = cmd:parse(arg)
local style_image_list = params.style_image:split(',')
if #style_image_list > 1 then
	local command = to_command(params)
	os.execute(command)
else
	local command = to_command(params)
	os.execute(command)
	local style_filename = style_image_list[1]
	local output_filename = params.output_image
	local ext = paths.extname(output_filename)
	local basename = paths.basename(output_filename, ext)
	local directory = paths.dirname(output_filename)
	local new_style_image_list = split(style_filename)
	for i = 1, #new_style_image_list do
		params.style_image = new_style_image_list[i]
		params.output_image = string.format("%s/%s_style%d.%s", directory, basename, i, ext)
		local command = to_command(params)
		os.execute(command)
	end
end
