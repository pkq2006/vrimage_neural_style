import numpy as np
import math
import scipy.misc
import matplotlib.pyplot as plt

img = scipy.misc.imread('out2_0.3_style2.jpg')
img2 = scipy.misc.imread('content.jpg')
plt.figure(1)
ax1 = plt.subplot(211)
ax2 = plt.subplot(212)
row_number = 320
for i in range(img.shape[2]):
    plt.figure(1)
    plt.sca(ax1)
    lst = []
    for j in range(img.shape[1] / 2 + 1, img.shape[1]):
        lst = lst + [img[row_number][j][i]]
    for j in range(img.shape[1] / 2 + 1):
        lst = lst + [img[row_number][j][i]]
    '''
    for j in range(img.shape[1]):
        lst = lst + [img[row_number][j][i]]
    '''
    plt.plot(range(img.shape[1]), lst)
    plt.sca(ax2)
    lst = []
    for j in range(img2.shape[1] / 2 + 1, img2.shape[1]):
        lst = lst + [img2[row_number][j][i]]
    for j in range(img2.shape[1] / 2 + 1):
        lst = lst + [img2[row_number][j][i]]
    '''
    for j in range(img2.shape[1]):
        lst = lst + [img2[row_number][j][i]]
    '''
    plt.plot(range(img2.shape[1]), lst)
plt.show()
